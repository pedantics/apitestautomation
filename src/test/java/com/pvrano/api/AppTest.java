package com.pvrano.api;

import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;

public class AppTest {

    @Test
    void itShouldSendPostRequest() {
        RestAssured.baseURI = "https://rahulshettyacademy.com";

        String body = "{\r\n" +
        "  \"location\": {\r\n" +
                "    \"lat\": -45.383494,\r\n" +
                "    \"lng\": 23.427362\r\n" +
                "  },\r\n" +
                "  \"accuracy\": 50,\r\n" +
                "  \"name\": \"Varun Shrivastava\",\r\n" +
                "  \"phone_number\": \"(+91) 996 054 3885\",\r\n" +
                "  \"address\": \"2647, Azad Nagar\",\r\n" +
                "  \"types\": [\r\n" +
                "    \"House Address\",\r\n" +
                "    \"Residence\"\r\n" +
                "  ],\r\n" +
                "  \"website\": \"http://google.com\",\r\n" +
                "  \"language\": \"French-IN\"\r\n" +
                "}";

        given()
                .queryParam("key", "qaclick123")
                .header("Content-Type", "application/json")
                .body(body)
                .when()
                .post("/maps/api/place/add/json")
                .then().assertThat().statusCode(200);
    }
}
